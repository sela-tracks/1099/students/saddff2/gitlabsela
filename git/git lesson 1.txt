REPOSITORY MANAGERS

MANAGE SECURITY
MANAGE BACKUPS
HIGH AVAILABILITY
MANAGE REPOSITORIES
GROUPS AND TEAMS
UI MANAGEMENT TOOLS
ISSUE TRACKING 
CODE REVIEW PROCESS
INTERGRATIONS


GIT BASICS

* git stores snapshots instead of deltas (saves only original files and adds changes in versions
						and saves more storage than old way)

* each developer has a copy of the entire repository (git pull from repository on private server or github, gitlab,
								bitbucket etc.)

* you can continue your work while been offline (you can git fetch all new updates while you've been offline
							and git push all your files to main)

* Brances are part of everyday development process (main and dev or build etc,.)

* merging is central to Git (don't be afraid of conflicts) (git knows how to merge 2 versions to one new)





GIT STRUCTURE 

git is not designed to be user friendly

git-push - Update remote refs along with associated objects

git-rm - Remove files from working tree and from the index

git-fetch Download objects and refs from another repository

git-reset - Reset current HEAD to the specified state



Git Structure - Meeting the SHA1

Is a hash function that convert as long string of data into a 40 character hexadecimal number

SHA1 = e89642b96685d5f22ee7044e05b9e6566e69b75
We'll work with first 5 digits (e8964)

Every object in Git have its own SHA1 (used as key)

each SHA1 is unique 

usually only the first 5 digits are used



GIT STRUCTURE - OBJECTS

	 (files)
	Blob (2f4t7)
     /		/----	Tree      ---- blob(1d9gs)
Commit -- Tree		(83e37)
(8e350) \ (d17b4)  \ ___ Blob (1b7f5)
	  (folders) \___ Blob (36c27)


GIT STRUCTURE - FILES STATUS LIFECYCLE

UNTRACKED	UNMODEFIED	MODIFIED	STAGED
		EDIT THE FILE->
 ADD THE FILE->			 STAGE THE FILE ->
 <-REMOVE THE LINE
		COMMIT <-------------------------COMMIT


BASIC COMMANDS

GIT INIT 	GIT COMMIT
GIT CLONE	GIT RM
GIT ADD		GIT MV


THE FOUR AREAS

STASH			   WORKING AREA		        INDEX			REPOSITORY
   |			(IDEs, notepad etc.)	     this is what's going 
   |						    to  next commit
Use git stash when you
 want to record the
 current state of
 the working directory 
and the index, but want
 to go back to a clean 
working directory.




you can abort the merge/rebase using

git merge --abort
git rebase --abort

GIT UNDOING CHANGES

git reset  -
git cherry-pick
git revert - undo 1 change (new file => git revert => deletes new file)(makes new commit with 
